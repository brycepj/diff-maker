const fs = require('fs');
const recursive = require('recursive-readdir');
const LineByLineReader = require('line-by-line');

const srcDir = '<EDIT-THIS-VALUE>';
const linesToMutatePerFile = 150; // EDIT THIS VALUE
const filesToMutate = 250; // EDIT THIS VALUE

const args = {
  srcDir,
  linesToMutatePerFile,
  filesToMutate,
};

function writeFileLineByLine(filePath, appendable) {
  fs.appendFile(filePath, appendable, () => {
    console.log(`appended to ${filePath}`);
  });
}

mutateShortlist(srcDir, args.filesToMutate);

function makeAppendable(lines) {
  const store = [];
  const baseAppendedLine = 'Line append to this file\n';

  while (store.length < lines) {
    store.push(`${store.length} ${baseAppendedLine}`);
  }

  return store.join('');
}

function mutateShortlist(src, filesToMutate) {
  return getFilesShortlistToMutate(src, filesToMutate)
    .then((shortlist) => {
      shortlist.forEach((filePath) => {
        const appendable = makeAppendable(args.linesToMutatePerFile);
        writeFileLineByLine(filePath, appendable);
      });
    });
}

function getFilesShortlistToMutate(src, number) {
  return getAllFiles(src).then((files) => {
    const totalFiles = files.length;
    const from = randomIndex(totalFiles, totalFiles - number);
    const to = from + number;
    return files.slice(from, to);
  })
}

function randomIndex(length, max) {
  const idx = Math.floor(Math.random()*length);
  return idx < max ? idx : randomIndex(length, max);
}

function getAllFiles(src) {
  return recursive(src)
    .catch((err) => console.error(err));
}

